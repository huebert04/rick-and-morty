import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePage from './components/HomePage/HomePage';
import CharacterInfo from './components/CharacterInfo/CharacterInfo';
import {BrowserRouter as Switch, Route} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" component={HomePage} exact></Route>
        <Route path="/character/:id" component={CharacterInfo} exact></Route>
      </Switch>
    </div>
  );
}

export default App;
