import React from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import styles from './CharacterInfo.module.css';
import logo from '../../rm_logo.png';

class CharacterInfo extends React.Component {
    render() {
        const {character} = this.props.location.state;
        return (
            <div className={styles.CharacterInfo}>
                <div className="logo">
                    <img src={logo} alt="" />
                </div>
                <Card className={styles.Card} border="secondary" >
                    <Card.Img src={character.image}></Card.Img>
                    <Card.Header>{character.name}</Card.Header>
                    <Card.Body>
                        <p>Status: {character.status ==="unknown" ? "Unknown Status" : character.status}</p>
                        <p>Species: {character.species === "unknown" ? "Unknown Species" : character.species}</p>
                        <p>Gender: {character.gender === "unknown" ? "Unknown Gender" : character.gender}</p>
                        <p>Origin: {character.origin.name ==="unknown" ? "Unknown Origin" : character.origin.name}</p>
                        <p>Current Location: {character.location.name === "unknown" ? "Unknown" : character.location.name}</p>
                    </Card.Body>
                    <Card.Footer>
                        <Link to ="/"><Button variant="primary">&larr; Go Back</Button></Link>
                    </Card.Footer>
                </Card>  
            </div>   
        );
    }
}

export default CharacterInfo;