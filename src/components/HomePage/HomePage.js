import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import CharacterList from "../CharacterList/CharacterList";
import { Pagination } from "react-bootstrap";
import styles from "./HomePage.module.css";

import logo from "../../rm_logo.png";
import { Form, FormControl } from "react-bootstrap";

export default () => {
  const [characters, setCharacters] = useState([]);
  const [url, setURL] = useState("https://rickandmortyapi.com/api/character");
  const [nextPage, setNextPage] = useState("");
  const [prevPage, setPrevPage] = useState("");

  const [filter, setFilter] = useState("");

  const getCharacters = url => {
    fetch(url)
      .then(resp => resp.json())
      .then(resp => {
        const chars = [];
        chars.push(...resp.results);
        setCharacters(chars);
        setNextPage(resp.info.next);
        setPrevPage(resp.info.prev);
      })
      .catch(err => console.log(err));
  };
  
  useEffect(() => {
    getCharacters(url);
  }, [url]);

  return (
    <div className="App">
      <div className="logo">
        <img src={logo} alt="" />
      </div>
      <Form style={{ margin: "30px auto", width: "25rem" }}>
        <FormControl
          type="text"
          placeholder="Search here"
          onChange={e => setFilter((e.target.value).toLowerCase())}
        />
      </Form>

      <CharacterList
        characters={characters.filter(item =>
          item.name.toLowerCase().includes(filter)
        )}
      />

      {!filter && (
        <Pagination className={styles.Pagination}>
          {prevPage && (
            <span onClick={() => setURL(prevPage)}>&laquo; Previous</span>
          )}
          <span onClick={() => setURL(nextPage)}>Next &raquo;</span>
        </Pagination>
      )}
    </div>
  );
};