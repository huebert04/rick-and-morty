import React from "react";
import styles from "./CharacterList.module.css";
import { CardDeck, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const CharacterList = props => {
  return (
    <div className={styles.CharacterList}>
      <CardDeck className={styles.CardDeck}>
        {props.characters.map(character => (
          <div key={character.id}>
            <Card className={styles.Card} border="secondary">
              <Card.Img variant="top" src={character.image} />
              <Card.Header>{character.name}</Card.Header>
              <Card.Footer>
                <Link
                  to={{
                    pathname: `/character/${character.id}`,
                    state: { character: character }
                  }}
                  key={character.id}
                >
                  <Button variant="primary">More Information</Button>
                </Link>
              </Card.Footer>
            </Card>
          </div>
        ))}
      </CardDeck>
      
    </div>
  );
};

export default CharacterList;
